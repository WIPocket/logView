const fs = require("fs");
const readline = require("readline");
const chalk = require("chalk");

const filter = require("./filter.json");

const hidden = require("./hidden.json");

const colors = [
  "red",
  "green",
  "cyan",
  "yellow",
  "magenta",
  "blueBright",
];

var unitLen;

init();

const rl = readline.createInterface({
  input: process.stdin,
  crlfDelay: Infinity,
  terminal: false,
});

var hiddenInARow = false;
rl.on('line', (line) => {
  var o = JSON.parse(line);
  
  if(!o._SYSTEMD_UNIT?.endsWith(".service")) return;
  
  var filterIndex = filter.indexOf(o._SYSTEMD_UNIT);
  if(filterIndex < 0) {
    filterIndex = filter.length;
    filter.push(o._SYSTEMD_UNIT);
    
    console.log(chalk.yellowBright(`Added ${o._SYSTEMD_UNIT} to filter at index ${filterIndex}`));
    fs.writeFileSync("./filter.json", JSON.stringify(filter, null, 2));
    console.log(chalk.yellowBright(`Wrote ./filter.json`));
    init();
  }
  
  if(hidden.includes(o._SYSTEMD_UNIT)) {
    if(!hiddenInARow) {
      console.log();
      hiddenInARow = {};
    }
    if(!hiddenInARow[o._SYSTEMD_UNIT])
      hiddenInARow[o._SYSTEMD_UNIT] = 0;
    hiddenInARow[o._SYSTEMD_UNIT]++;  
    process.stdout.moveCursor(0, -1);
    console.log(
      chalk.gray(
        Object.keys(hiddenInARow)
          .map((unit, i) => `${hiddenInARow[unit]} ${i == 0 ? "lines hidden " : ""}from ${unit}`)
          .join(", ")
        )
    );
    return;
  } else hiddenInARow = false;
      
  var time = new Date(o.__REALTIME_TIMESTAMP / 1000).toLocaleString("cs").padStart(22);
  var unit = o._SYSTEMD_UNIT.replace(".service", "").padStart(unitLen - 7);
  var message = typeof o.MESSAGE == "string" ? o.MESSAGE : String.fromCharCode(...o.MESSAGE);
  var sev = Number(o.PRIORITY);
  var severity = `|${sev}|`;
       if(sev <= 2) sev = chalk.redBright(severity);
  else if(sev <= 3) sev = chalk.yellow(severity);
  else if(sev <= 5) sev = chalk.white(severity);
  else sev = chalk.gray(severity);
  
  console.log(`${sev}${chalk.gray(time)} ${chalk[colors[filterIndex]](unit)}: ${message}`);
});

function init() {
  while(colors.length < filter.length)
    colors.forEach(color => colors.push(color));
  
  unitLen = filter.reduce((a,c) => Math.max(a, c.length), 0);
}
